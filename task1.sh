#!/usr/bin/env bash
#Script for training in ACL
PROJ_HOME="/home/projects"
echo "Start task 1"
#Check rights
if [ "$(whoami)" != "root" ]; then echo "Error! You are not root!" && exit 1; fi
#Test needed utils
if $(setfacl -v >/dev/null);then : ; else echo "Error! You need to install package 'acl'!" && exit 1; fi
#Create directories for projects and create groups
for PROJ in Proj{1..3}; do
  mkdir -m 750 -p $PROJ_HOME/$PROJ
  if [[ "$(grep -c "^$PROJ:" /etc/group)" -eq 0 ]]; then
    groupadd $PROJ
  fi
done
#Create users
for USER in R1 R2 R3 R4 R5 A1 A2 A3 A4 I1 I2 I3; do
  if [[ "$(grep -c "^$USER:" /etc/passwd)" -eq 0 ]]; then
    useradd -s $(which bash) -d $PROJ_HOME $USER
    PASSWORD=$(tr -dc A-Za-z0-9 < /dev/urandom 2>/dev/null| head -c8)
    echo "$USER:$PASSWORD" | chpasswd
    echo "User: $USER Password: $PASSWORD"
  fi
done
#Add users in groups
for GROUP_1 in R2 R3 R5 A1; do
   usermod -a -G Proj1 $GROUP_1
done
for GROUP_2 in  R1 R5 A1; do
   usermod -a -G Proj2 $GROUP_2
done
for GROUP_3 in  R1 R2 R4 A2; do
   usermod -a -G Proj3 $GROUP_3
done
#Add rights for users ang groups
setfacl -R -m d:g:Proj1:rwx,g:Proj1:rwx  $PROJ_HOME/Proj1
setfacl -R -m d:g:Proj2:rwx,g:Proj2:rwx  $PROJ_HOME/Proj2
setfacl -R -m d:g:Proj3:rwx,g:Proj3:rwx  $PROJ_HOME/Proj3
setfacl -R -m d:u:A4:rx,u:A4:rx  $PROJ_HOME/Proj1
setfacl -R -m d:u:A2:rx,u:A2:rx  $PROJ_HOME/Proj2
setfacl -R -m d:u:A3:rx,u:A3:rx  $PROJ_HOME/Proj2
setfacl -R -m d:u:A1:rx,u:A1:rx  $PROJ_HOME/Proj3
setfacl -R -m d:u:A4:rx,u:A4:rx  $PROJ_HOME/Proj3
echo "End task 1"

echo "Start task 2"
#Add rights for INFO managers:
if [[ "$(grep -c "^info:" /etc/group)" -eq 0 ]]; then
  groupadd info
fi
for GROUP_4 in  I1 I2 I3; do
   usermod -a -G info $GROUP_4
done
setfacl -R -m d:g:info:rx,g:info:rx  $PROJ_HOME/Proj1
setfacl -R -m d:g:info:rx,g:info:rx  $PROJ_HOME/Proj2
setfacl -R -m d:g:info:rx,g:info:rx  $PROJ_HOME/Proj3
if [[ "$(grep -c "^%info" /etc/sudoers)" -eq 0 ]]; then
  echo "%info   ALL=(ALL) NOPASSWD: $(which touch)" >> /etc/sudoers
fi
echo "Info managers can create files with 'sudo touch'"
echo "End task 2"
